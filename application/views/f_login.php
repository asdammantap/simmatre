<html>
<head>
<title>MATRE</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="mantap reload">
<meta name="keyword" content="pulsa">
<meta name="author" content="Wong Mantap">
<script src="assets/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
<script src="assets/js/login.js"></script>
<link rel="stylesheet" href="assets/css/login.css">  
<link rel="icon" type="image/png" id="favicon"
          href="assets/image/mantapreloadbulatweblagi.png"/>
</head>
<body>
    <div class="container">
	<div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="assets/image/mantapreloadbulatweblagi.png" />
            <p id="profile-name" class="profile-name-card"></p>
			<?=$this->session->flashdata('pesan')?>
            <?php echo form_open("sync/cek_loginadmin"); ?>
                <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
                </br><input type="password" class="form-control" placeholder="Password" name="passwd" required>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" name="remember_me" value="remember-me">Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
				<?php echo form_close(); ?>
           <a href="forgot" class="forgot-password">
                Forgot the password?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
</html>
