<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="mantap reload">
<meta name="keyword" content="pulsa">
<meta name="author" content="Wong Mantap">
<title>MATRE - Dashboard</title>
<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/datepicker3.css" rel="stylesheet">
<link href="../assets/css/styles.css" rel="stylesheet">
<link rel="icon" type="image/png" id="favicon"
          href="../assets/image/mantapreloadbulatweblagi.png"/>
<!--Icons-->
<script src="../assets/js/lumino.glyphs.js"></script>
<!--[if lt IE 9]>
<script src="../assets/js/html5shiv.js"></script>
<script src="../assets/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Admin</span>MATRE</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="./logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="dashboard"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="./daftarharga"><svg class="glyph stroked calendar"><use xlink:href="#stroked-table"></use></svg> Daftar Harga</a></li>
			<li><a href="./saldo"><svg class="glyph stroked tag"><use xlink:href="#stroked-tag"></use></svg> Saldo</a></li>
			<li><a href="./pulsa"><svg class="glyph stroked mobile device"><use xlink:href="#stroked-mobile-device"></use></svg> Pulsa</a></li>
			<li class="active"><a href="./token"><svg class="glyph stroked desktop"><use xlink:href="#stroked-desktop"></use></svg> Token</a></li>
			<li><a href="./tampillaporan"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg> Laporan</a></li>
			<li role="presentation" class="divider"></li>
			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Downline Area 
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="./listdl">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> List Downline
						</a>
					</li>
					</ul>
			</li>
		</ul>

	</div><!--/.sidebar-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked mobile device"><use xlink:href="#stroked-mobile-device"></use></svg></a></li>
				<li class="active">Transaksi Token</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Form Transaksi Token</div>
					<div class="panel-body">
						<div class="col-md-6">
						<?=$this->session->flashdata('pesan')?>
						<form action="<?=base_url()?>home/transpulsa" method="post" enctype="multipart/form-data" role="form">
							<div class="form-group">
									<label>No. Transaksi</label>
									<input type="text" name="notransaksi" class="form-control" value="" placeholder="No. Transaksi">
								</div>
								<div class="form-group">
									<label>Tanggal Transaksi</label>
									<input type="date" name="tgl_transaksi" class="form-control" value="" placeholder="">
								</div>
								<div class="form-group">
									<label>No. Token</label>
									<input type="text" name="nohp" class="form-control" value="" placeholder="No. Handphone">
								</div>
								<div class="form-group">
									<label>Kode Token</label>
									<select class="form-control" name="kd_voucher">
										<option value=''>--Pilih Kode Token--</option>
										<option value=<?=form_dropdown('kd_voucher',$tampilkdtoken);?></option>
									</select>
								</div>
								<div class="form-group">
									<label>Kode Deposit</label>
									<input type="text" name="kd_depo" class="form-control" value="" placeholder="Kode Deposit">
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<div class="radio">
										<label>
											<input type="radio" name="keterangan" id="optionsRadios1" value="LUNAS" checked>Bayar
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="keterangan" id="optionsRadios2" value="BELUM LUNAS">Hutang
										</label>
									</div>
								</div>
								<button type="submit" class="btn btn-primary">Submit Button</button>
								<button type="reset" class="btn btn-default">Reset Button</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	<script src="../assets/js/jquery-1.11.1.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/chart.min.js"></script>
	<script src="../assets/js/chart-data.js"></script>
	<script src="../assets/js/easypiechart.js"></script>
	<script src="../assets/js/easypiechart-data.js"></script>
	<script src="../assets/js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
