﻿<?php
class PDF extends FPDF
{
	//Page header
	function Header()
	{
                $this->setFont('Arial','',10);
                $this->setFillColor(255,255,255);
                $this->cell(100,6,"Laporan Transaksi Pulsa",0,0,'L',1); 
                $this->cell(0,6,"Printed date : " . date('d/m/Y'),0,1,'R',1); 
                $this->Image(base_url().'assets/image/mantapreloadbulatweblagi.png', 10, 25,'20','20','png');
                
                $this->Ln(12);
                $this->setFont('Arial','',14);
                $this->setFillColor(255,255,255);
                $this->cell(120,6,'',0,0,'C',0); 
                $this->cell(100,6,'Laporan Transaksi Pulsa',0,1,'C',1); 
                $this->cell(120,6,'',0,0,'C',0); 
                $this->cell(100,6,"Periode : ".date('M Y'),0,1,'C',1); 
                $this->cell(25,6,'',0,0,'C',0); 
                
                $this->Ln(15);
                $this->setFont('Arial','',12);
                $this->setFillColor(0,146,63);
				$this->setTextColor(255,255,255);
                $this->cell(10,10,'No.',1,0,'C',1);
                $this->cell(50,10,'Tanggal',1,0,'C',1);
                $this->cell(80,10,'No. HP',1,0,'C',1);
                $this->cell(80,10,'Jenis Pulsa',1,0,'C',1);
				$this->cell(50,10,'No. Deposit',1,0,'C',1);
				$this->cell(50,10,'Keterangan',1,0,'C',1);
				$this->Ln();
				
	}
 
	function Content($data)
	{
            $no = 1;
                foreach ($data as $key) {
                        $this->setFont('Arial','',12);
                        $this->setFillColor(255,255,255);	
                        $this->cell(10,10,$no,1,0,'L',1);
                        $this->cell(50,10,$key->tgl_transaksi,1,0,'L',1);
                        $this->cell(80,10,$key->nohp,1,0,'L',1);
                        $this->cell(80,10,$key->ket_voucher,1,0,'L',1);
						$this->cell(50,10,$key->kd_depo,1,0,'L',1);
						$this->cell(50,10,$key->keterangan,1,0,'L',1);
						$no++;
						$this->Ln();
                }            

	}
	function Footer()
	{
		//atur posisi 1.5 cm dari bawah
		$this->SetY(-15);
		//buat garis horizontal
		$this->Line(10,$this->GetY(),340,$this->GetY());
		//Arial italic 9
		$this->SetFont('Arial','I',9);
                $this->Cell(0,10,'MATRE ' . date('Y'),0,0,'P');
		//nomor halaman
		$this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($data);
$pdf->Output();