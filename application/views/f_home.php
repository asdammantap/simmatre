<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="mantap reload">
<meta name="keyword" content="pulsa">
<meta name="author" content="Wong Mantap">
<title>MATRE - Dashboard</title>
<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/datepicker3.css" rel="stylesheet">
<link href="../assets/css/styles.css" rel="stylesheet">
<link rel="icon" type="image/png" id="favicon"
          href="../assets/image/mantapreloadbulatweblagi.png"/>
<!--Icons-->
<script src="../assets/js/lumino.glyphs.js"></script>
<!--[if lt IE 9]>
<script src="../assets/js/html5shiv.js"></script>
<script src="../assets/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Admin</span>MATRE</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="./profil"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="./logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="dashboard"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="./daftarharga"><svg class="glyph stroked calendar"><use xlink:href="#stroked-table"></use></svg> Daftar Harga</a></li>
			<li><a href="./saldo"><svg class="glyph stroked tag"><use xlink:href="#stroked-tag"></use></svg> Saldo</a></li>
			<li><a href="./pulsa"><svg class="glyph stroked mobile device"><use xlink:href="#stroked-mobile-device"></use></svg> Pulsa</a></li>
			<li><a href="./token"><svg class="glyph stroked desktop"><use xlink:href="#stroked-desktop"></use></svg> Token</a></li>
			<li><a href="./tampillaporan"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg> Laporan</a></li>
			<li role="presentation" class="divider"></li>
			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Downline Area 
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="./listdl">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> List Downline
						</a>
					</li>
					</ul>
			</li>
		</ul>

	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		<div class="row">
		<div class="col-xs-12 col-md-6 col-lg-4">
				<div class="panel panel-blue panel-widget ">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-2 widget-left">
							<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large"><?php 
							foreach ($tampilpelanggan as $row) {
							echo $row->jumlah_pelanggan;}?></div>
							<div class="text-muted">Customer</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-4">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-2 widget-left">
							<svg class="glyph stroked bag "><use xlink:href="#stroked-bag"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">Rp. <?php 
							foreach ($tampilsaldo as $row) {
							$formatsaldo=$row->laba;
							echo number_format($formatsaldo,0,",",".");}?></div>
							<div class="text-muted">Profit Saat Ini</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-4">
				<div class="panel panel-red panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-2 widget-left">
							<svg class="glyph stroked tag"><use xlink:href="#stroked-tag"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">Rp. <?php 
							foreach ($tampilsaldo as $row) {
							$formatsaldo=$row->saldo;
							echo number_format($formatsaldo,0,",",".");}?></div>
							<div class="text-muted">Saldo Saat Ini</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body tabs">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">Pembeli Terbanyak</a></li>
							<li><a href="#tab2" data-toggle="tab">Provider Terbanyak</a></li>
							<li><a href="#tab3" data-toggle="tab">Belum Bayar</a></li>
						</ul>
		
						<div class="tab-content">
							<div class="tab-pane fade in active" id="tab1">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">Pelanggan Yang Paling Banyak Mengisi Pulsa</div>
										<div class="panel-body"></div>
								<div class="panel-body">
						<table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="Jumlah_Isi" data-sort-order="desc">
						    <thead>
						    <tr>
						        <!--<th data-field="state" data-checkbox="true" >Kode Voucher</th>-->
						        <th data-field="nohp" data-sortable="">No. HP</th>
								<th data-field="Jumlah_Isi"  data-sortable="">Banyak Pengisian</th>
						    </tr>
							</thead>
							<tbody>
								<tr>
							<?php 
							foreach ($tampilpelangganmax as $row) {?>	 	
									<td> <?=$row->nohp;?></td>
									<td> <?=$row->Jumlah_Isi;?></td>
								</tr>
							<?php
								}
							?>
							</tbody>
						</table>
						</div>
						</div>
						</div>
						</div>
							<div class="tab-pane fade" id="tab2">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">Kartu Yang Paling Banyak Digunakan</div>
										<div class="panel-body"></div>
								<div class="panel-body">
						<table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="banyakdigunakan" data-sort-order="desc">
						    <thead>
						    <tr>
						        <!--<th data-field="state" data-checkbox="true" >Kode Voucher</th>-->
								<th data-field="provider" data-sortable="">Provider</th>
								<th data-field="banyakdigunakan"  data-sortable="">Banyak Penggunaan</th>
						    </tr>
							</thead>
							<tbody>
								<tr>
							<?php 
							foreach ($tampilprovidermax as $row) {?>	 
									<td> <?=$row->provider;?></td>
									<td> <?=$row->banyakdigunakan;?></td>
								</tr>
							<?php
								}
							?>
							</tbody>
						</table>
						</div>
						</div>
						</div>
							</div>
							<div class="tab-pane fade" id="tab3">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">Pelanggan Yang Belum Melunasi Pembayaran</div>
										<div class="panel-body"></div>
								<div class="panel-body">
						<table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="Jumlah_Isi" data-sort-order="">
						    <thead>
						    <tr>
						        <!--<th data-field="state" data-checkbox="true" >Kode Voucher</th>-->
								<th data-field="tgl_transaksi" data-sortable="">Tanggal</th>
						        <th data-field="nohp" data-sortable="">No. HP</th>
								<th data-field="Jumlah_Isi"  data-sortable="">Voucher</th>
						    </tr>
							</thead>
							<tbody>
								<tr>
							<?php 
							foreach ($tampilbelumlunas as $row) {?>	 
									<td> <?=$row->tgl_transaksi;?></td>
									<td> <?=$row->nohp;?></td>
									<td> <?=$row->kd_voucher;?></td>
								</tr>
							<?php
								}
							?>
							</tbody>
						</table>
						</div>
						</div>
						</div>
							</div>
						</div>
					</div>
				</div><!--/.panel-->
			</div><!--/.col-->
				<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">Traffic Jenis Kartu Yang Digunakan Pelanggan</div>
					<div class="panel-body">
						
				     <iframe src="../chart" scrolling="none" frameborder="0" width="100%" height="430px"></iframe>
			
				</div>
				</div>
			</div>
		</div>
			</div>	<!--/.main-->
	<script src="../assets/js/jquery-1.11.1.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/chart.min.js"></script>
	<script src="../assets/js/chart-data.js"></script>
	<script src="../assets/js/easypiechart.js"></script>
	<script src="../assets/js/easypiechart-data.js"></script>
	<script src="../assets/js/bootstrap-datepicker.js"></script>
	<script src="../assets/js/bootstrap-table.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>
</html>
