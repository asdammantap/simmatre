<?php 
	Class modul_saldo extends CI_Model {

	var $tblsaldo = 'saldo';
	
		Function tampilsaldo()
	{
		$this->db->where('no_depo',$this->session->userdata('no_depo'));
		$query=$this->db->get('view_historysaldo');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	function report(){
        $query=$this->db->get('view_banyakprovider');
		if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
	public function get_nodepo() {
  $tahun = date("Ymd");
  $kode = 'D';
  $query = $this->db->query("SELECT MAX(no_depo) as autonodepo FROM saldo"); 
  $row = $query->row_array();
  $autono_depo = $row['autonodepo']; 
  $autono_depo1 =(int) substr($autono_depo,9,5);
  $no_deposit = $autono_depo1 +1;
  $autonodepo = $kode.$tahun;
  return $autonodepo;
 }
	function get_insertsaldo($data){
       $this->db->insert($this->tblsaldo, $data);
       return TRUE;
    }
	Function tampiljumlahpelanggan()
	{
		$query=$this->db->get('view_jumlahcus');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampilbelumbayar()
	{
		$query=$this->db->get('view_belumlunas');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampiljumlahpelangganmax()
	{
		$query=$this->db->get('view_maxfinal');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampiljumlahprovidermax()
	{
		$query=$this->db->get('view_maxprovider');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampilsaldoakhir()
	{
		$this->db->where('no_depo',$this->session->userdata('no_depo'));
		$query=$this->db->get('view_historysaldotampil');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampilsaldodl()
	{
		$this->db->where('kd_voucher',$this->session->userdata('kd_voucher'));
		$query=$this->db->get('view_saldodl');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
		
}