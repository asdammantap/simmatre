<?php 
	Class modul_pulsa extends CI_Model {

	var $tbltranspulsa = 'd_trans';
	
    function __construct() {
        parent::__construct();
    }
	function get_inserttranspulsa($data){
       $this->db->insert($this->tbltranspulsa, $data);
       return TRUE;
    }
	
	public function get_notrans() {
  $tahun = date("Ym");
  $kode = 'P';
  $query = $this->db->query("SELECT MAX(notransaksi) as autonotrans FROM d_trans"); 
  $row = $query->row_array();
  $autono_trans = $row['autonotrans']; 
  $autono_trans1 =(int) substr($autono_trans,1,10);
  $no_trans = $autono_trans1 + 1;
  $autonotrans = $kode.$no_trans;
  return $autonotrans;
 }
	function moduleditsaldo() { 
		
			$id = $this->input->post('kd_voucher'); 
			$data = array(
				  'kd_voucher' =>$this->input->post('kd_voucher'),
				  'harga_server' =>$this->input->post('harga_server'),
				  'harga_jual' =>$this->input->post('harga_jual')
				  );
				$this->db->where('kd_voucher',$id); 
            $this->db->update('harga_voucher',$data); 
		}
	function moduleditpulsa() { 
		
			$id = $this->input->post('notransaksi'); 
			$data = array(
				  'nohp' =>$this->input->post('nohp'),
				  'kd_voucher' =>$this->input->post('kd_voucher'),
				  'keterangan' =>$this->input->post('keterangan')
				  );
				$this->db->where('notransaksi',$id); 
            $this->db->update('d_trans',$data); 
		}
	function getkdvoucher()
	{
		$query = $this->db->query("SELECT * FROM voucher");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['kd_voucher']] = $row['ket_voucher'];
	}
	}
	return $data;
	}	
	Function get_editpulsa($id)
	{
		 $this->db->where('notransaksi',$id); 
         $query = $this->db->get('view_transpulsa'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function tampiltranspulsa()
	{
		$query=$this->db->get('view_transpulsa');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampillaporan()
	{
		//$this->db->where('bulan',10);
		$query=$this->db->get('view_laporanpulsa');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
}
?>