<?php 
	Class modul_pl extends CI_Model {

	var $tblvoucher = 'voucher';
	var $tblhargavoucher = 'harga_voucher';
	
		Function tampilpl()
	{
		$this->db->where('kd_voucher',$this->session->userdata('kd_voucher'));
		$query=$this->db->get('view_hargavoucher');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function get_detailharga($id)
	{
		 $this->db->where('kd_voucher',$id); 
         $query = $this->db->get('view_hargavoucher'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	function getkdprovider()
	{
		$query = $this->db->query("SELECT * FROM dt_provider");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['kd_provider']] = $row['provider'];
	}
	}
	return $data;
	}
	function get_insertvoucher($data){
       $this->db->insert($this->tblvoucher, $data);
       return TRUE;
    }
	function get_inserthargavoucher($dataharga){
       $this->db->insert($this->tblhargavoucher, $dataharga);
       return TRUE;
    }
	Function get_editharga($id)
	{
		 $this->db->where('kd_voucher',$id); 
         $query = $this->db->get('view_hargavoucher'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	function moduleditharga() { 
		
			$id = $this->input->post('kd_voucher'); 
			$data = array(
				  'kd_voucher' =>$this->input->post('kd_voucher'),
				  'harga_server' =>$this->input->post('harga_server'),
				  'harga_jual' =>$this->input->post('harga_jual')
				  );
				$this->db->where('kd_voucher',$id); 
            $this->db->update('harga_voucher',$data); 
		}
		public function hapus_voucher($id){ 
			
			$this->db->where('kd_voucher',$id);
			$query = $this->db->get('voucher');
			$row = $query->row();
			$this->db->delete('voucher', array('kd_voucher' => $id));

		}
		public function hapus_harga($id){ 
			
			$this->db->where('kd_voucher',$id);
			$query = $this->db->get('harga_voucher');
			$row = $query->row();
			$this->db->delete('harga_voucher', array('kd_voucher' => $id));

		}
	Function tampilpldl()
	{
		$this->db->where('kd_voucher',$this->session->userdata('kd_voucher'));
		$query=$this->db->get('view_hargavoucherdl');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
		
}