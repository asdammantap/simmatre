<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sync extends CI_Controller {
public function index($error = NULL) {
        $data = array(
            'title' => 'Login Page',
            'action' => site_url('sync/login'),
            'error' => $error
        );
        $this->load->view('login', $data);
    }
public function cek_loginadmin() {
		$data1 = array('username' => $this->input->post('username', TRUE),
						'passwd' => md5($this->input->post('passwd', TRUE))
			);
		$this->load->model('modul_login'); // load model_user
		$hasil = $this->modul_login->cek_user($data1);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['remember_me'] = $this->input->post('remember_me');
				$sess_data['username'] = $sess->username;
				$sess_data['passwd'] = $sess->passwd;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
			//redirect('../home/dashboard');
		if ($this->session->userdata('level')=='Super Admin') {
				redirect('../home/dashboard');
			}
			elseif ($this->session->userdata('level')=='Downline') {
				foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['remember_me'] = $this->input->post('remember_me');
				$sess_data['username'] = $sess->username;
				$sess_data['passwd'] = $sess->passwd;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
				redirect('../home/dashboarddl');
			}
		}
		else {
			$this->session->set_flashdata("pesan",'
			 	<div class="alert alert-danger alert-dismissible" role="alert">
				  Username/Password Anda Salah !
				</div>
			 	');
            redirect('../'); 
		}
	}
	function forgot() { 
		$this->load->model('modul_login','',TRUE); 
            $this->modul_login->modulubahpass(); 
			$this->session->set_flashdata("pesan",'
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  Password Berhasil Diubah !</br>
				  Silahkan Login Kembali !
				</div>
			 	');
            redirect('../'); }
}
?>
