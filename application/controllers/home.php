<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('modul_pulsa'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 
		$this->load->library('fpdf');
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function dashboard()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_saldo');
			$data['report'] = $this->modul_saldo->report();
			$data['tampilprovidermax']=$this->modul_saldo->tampiljumlahprovidermax();
			$data['tampilbelumlunas']=$this->modul_saldo->tampilbelumbayar();
			$data['tampilpelangganmax']=$this->modul_saldo->tampiljumlahpelangganmax();
			$data['tampilpelanggan']=$this->modul_saldo->tampiljumlahpelanggan();
			$data['tampilsaldo']=$this->modul_saldo->tampilsaldoakhir();
			$this->load->view('f_home',$data);
		}
		else {
			redirect('');
		}
	}
	public function setting()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_saldo');
			$data['report'] = $this->modul_saldo->report();
			$data['tampilprovidermax']=$this->modul_saldo->tampiljumlahprovidermax();
			$data['tampilbelumlunas']=$this->modul_saldo->tampilbelumbayar();
			$data['tampilpelangganmax']=$this->modul_saldo->tampiljumlahpelangganmax();
			$data['tampilpelanggan']=$this->modul_saldo->tampiljumlahpelanggan();
			$data['tampilsaldo']=$this->modul_saldo->tampilsaldoakhir();
			$this->load->view('f_home',$data);
		}
		else {
			redirect('');
		}
	}
	public function profil()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_user');
		$data['data']=$this->modul_user->get_profil();
		$this->load->view('f_profil',$data);
		}
		else {
			redirect('');
		}
	}
	public function dashboarddl()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$this->load->view('f_homedl');
		}
		else {
			redirect('');
		}
	}
	public function listdl()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_dl');
		$data['data']=$this->modul_dl->tampillistdl();
		$this->load->view('f_listdl',$data);
		}
		else {
			redirect('');
		}
	}
	public function addprice()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pl');
		$data['tampilkdprovider']=$this->modul_pl->getkdprovider();
		$this->load->view('f_addpricelist',$data);
		}
		else {
			redirect('');
		}
	}
	public function addsaldo()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_saldo');
		$data['no_deposit']=$this->modul_saldo->get_nodepo();
		$this->load->view('f_addsaldo',$data);
		}
		else {
			redirect('');
		}
	}
	public function insertprice(){
		$data = array(
				  'kd_voucher' =>$this->input->post('kd_voucher'),
				  'ket_voucher' =>$this->input->post('ket_voucher'),
				  'kd_provider' =>$this->input->post('kd_provider'));
		$dataharga = array(
				  'kd_voucher' =>$this->input->post('kd_voucher'),
				  'harga_server' =>$this->input->post('harga_server'),
				  'harga_jual' =>$this->input->post('harga_jual'));
		$this->load->model('modul_pl');
		$this->modul_pl->get_insertvoucher($data); //akses model untuk menyimpan ke database
		$this->modul_pl->get_inserthargavoucher($dataharga); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Berhasil !!</div></div>");
                redirect('../home/addprice'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Gagal !!</div></div>");
                redirect('../home/addprice'); //jika gagal maka akan ditampilkan form upload
	}         
    }
	public function insertsaldo(){
		$tgl=$this->input->post('tgl_depo');
		$tgl1=date('d-m-Y', strtotime($tgl));
		$data = array(
				  'no_depo' =>$this->input->post('no_depo'),
				  'tgl_depo' =>$tgl1,
				  'nmnal_depo' =>$this->input->post('nmnal_depo'),
				  'saldo' =>$this->input->post('nmnal_depo'));
		$this->load->model('modul_saldo');
		$this->modul_saldo->get_insertsaldo($data);
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Input Data Berhasil !!</div></div>");
                redirect('../home/addsaldo'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Input Data Gagal !!</div></div>");
                redirect('../home/addsaldo'); //jika gagal maka akan ditampilkan form upload
	}         
    }
	public function daftarharga()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pl');
		$data['data']=$this->modul_pl->tampilpl();
		$this->load->view('f_pricelist',$data);
		}
		else {
			redirect('');
		}
	}
	public function detailharga($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pl');
		$data['data']=$this->modul_pl->get_detailharga($id);
		$this->load->view('f_detailpricelist',$data);
		}
		else {
			redirect('');
		}
	}
	public function editharga($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pl');
		$data['data']=$this->modul_pl->get_editharga($id);
		$this->load->view('f_editpricelist',$data);
		}
		else {
			redirect('');
		}
	}
	function proseseditharga() { 
		$this->load->model('modul_pl','',TRUE); 
            $this->modul_pl->moduleditharga(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../home/daftarharga');
        }
		public function deleteharga($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_pl','',TRUE); 
		$data['data']=$this->modul_pl->hapus_voucher($id);
		$data['data']=$this->modul_pl->hapus_harga($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../home/daftarharga');
            }
		$this->load->view('home/daftarharga', $data);
	}
	public function saldo()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_saldo');
		$data['data']=$this->modul_saldo->tampilsaldo();
		$data['tampilpelanggan']=$this->modul_saldo->tampiljumlahpelanggan();
		$data['tampilsaldo']=$this->modul_saldo->tampilsaldoakhir();
		$this->load->view('f_mysaldo',$data);
		}
		else {
			redirect('');
		}
	}
	public function daftarhargadl()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pl');
		$data['data']=$this->modul_pl->tampilpldl();
		$this->load->view('f_pricelistdl',$data);
		}
		else {
			redirect('');
		}
	}
	public function pulsa()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pulsa');
		$data['data']=$this->modul_pulsa->tampiltranspulsa();
		$data['no_trans']=$this->modul_pulsa->get_notrans();
		$data['tampilkdvoucher']=$this->modul_pulsa->getkdvoucher();
		$this->load->view('f_tpulsa',$data);
		}
		else {
			redirect('');
		}
	}
	public function statuspulsa($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pulsa');
		$data['data']=$this->modul_pulsa->tampiltranspulsa();
		$data['data']=$this->modul_pulsa->get_editpulsa($id);
		$data['no_trans']=$this->modul_pulsa->get_notrans();
		$data['tampilkdvoucher']=$this->modul_pulsa->getkdvoucher();
		$this->load->view('f_tpulsaedit',$data);
		}
		else {
			redirect('');
		}
	}
	function proseseditpulsa() { 
		$this->load->model('modul_pl','',TRUE); 
            $this->modul_pulsa->moduleditpulsa(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../home/pulsa');
        }
	public function token()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_token');
		$data['tampilkdtoken']=$this->modul_token->getkdtoken();
		$this->load->view('f_ttoken',$data);
		}
		else {
			redirect('');
		}
	}
	public function transpulsa(){
		$tgl=$this->input->post('tgl_transaksi');
		$tgl1=date('d-m-Y', strtotime($tgl));
		$data = array(
				  'notransaksi' =>$this->input->post('notransaksi'),
				  'tgl_transaksi' =>$tgl1,
				  'nohp' =>$this->input->post('nohp'),
				  'kd_voucher' =>$this->input->post('kd_voucher'),
				  'kd_depo' =>$this->input->post('kd_depo'),
				  'keterangan' =>$this->input->post('keterangan')
				  );
		$this->modul_pulsa->get_inserttranspulsa($data); //akses model untuk menyimpan ke database
		if($this->form_validation->run()==FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Proses Transaksi Berhasil !!</div></div>");
                redirect('../home/pulsa'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Proses Transaksi Gagal !!</div></div>");
                redirect('../home/pulsa'); //jika gagal maka akan ditampilkan form upload
	}         
    }
	public function tampillaporan()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_pulsa');
		$data['data']=$this->modul_pulsa->tampillaporan();
		$this->load->view('f_laporan',$data);
		}
		else {
			redirect('');
		}
	}
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}
}

# nama file home.php
# folder apllication/controller/